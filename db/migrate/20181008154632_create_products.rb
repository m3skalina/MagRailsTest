class CreateProducts < ActiveRecord::Migration[5.2]
  def change
    create_table :products do |t|
      t.string :name
      t.text :description
      t.float :price
      t.integer :qtystock, default: 0
      t.references :user, foreign_key: true

      t.timestamps
    end
    add_index :products, [:user_id, :created_at]
  end
end
