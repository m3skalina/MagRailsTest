class AddDiscardedAtToMovements < ActiveRecord::Migration[5.2]
  def change
    add_column :movements, :discarded_at, :datetime
    add_index :movements, :discarded_at
  end
end
