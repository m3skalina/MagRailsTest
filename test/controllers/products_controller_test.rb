require 'test_helper'

class ProductsControllerTest < ActionDispatch::IntegrationTest

  def setup
    @product = products(:one)
    @user = users(:michael)
    @admin = users(:fabio)
  end

  test "should redirect create when not logged in" do
    assert_no_difference 'Product.count' do
      post products_path(@product)
    end
    assert_redirected_to login_path
  end

  test "should redirect destroy when not logged in" do
    assert_no_difference 'Product.count' do
      delete product_path(@product)
    end
    assert_redirected_to login_path
  end

  test "should redirect destroy when loggen in as non-admin user" do
    log_in_as(@user)
    assert_no_difference 'Product.count' do
      delete product_path(@product)
    end
    assert_redirected_to products_url
  end
end
