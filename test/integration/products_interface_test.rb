require 'test_helper'

class ProductsInterfaceTest < ActionDispatch::IntegrationTest

  def setup
    @user = users(:michael)
    @fabio = users(:fabio)
    @product = products(:one)
  end

  test "product interface" do
    log_in_as(@user)
    get products_path
    assert_select 'div.pagination'
    # invalid submission
    get new_product_path
    assert_no_difference 'Product.count' do
      post products_path, params: { product: {
        name: ''
      }}
    end
    assert_select 'div#error_explanation'
    # valid submission
    get new_product_path
    picture = fixture_file_upload('test/fixtures/files/btn-menu.png', 'image/png')
    assert_difference 'Product.count', 1 do
      post products_path, params: { product: {
          name: 'Product 001',
          description: 'Description',
          price: 1.2,
          qtystock: 100,
          product_code: 'AA001',
          picture: picture
        }}
    end
    assert_redirected_to products_path
    # view product
    get product_url(@product)
    assert_select 'div.panel'
    # view index of users by non-admin without delete button
    get products_url
    assert_select 'a', text: 'delete', count: 0
    # view index of users by admin with delete button
    log_in_as(@fabio)
    get products_url
    assert_select 'a', text: 'Delete'
  end
end
