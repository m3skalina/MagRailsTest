require 'test_helper'

class ProductTest < ActiveSupport::TestCase

  def setup
    @user = users(:michael)
    @product = @user.products.new(
      name: 'Product Demo',
      description: 'Description',
      price: 10,
      qtystock: 1,
      product_code: 'A001'
    )
  end

  test "should be valid" do
    assert @product.valid?
  end

  test "user id should be present" do
    @product.user_id = nil
    assert_not @product.valid?
  end

  test "product name should be present" do
    @product.name = "   "
    assert_not @product.valid?
  end
end
