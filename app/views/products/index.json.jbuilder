# frozen_string_literal: true

json.array!(@products) do |product|
  json.id           product.id
  json.name         product.name
  json.product_code product.product_code
  json.picture      product.picture
end
