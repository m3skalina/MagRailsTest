# frozen_string_literal: true

module SessionsHelper
  # log in the user
  def log_in(user)
    session[:user_id] = user.id
  end

  # remember a user in a persistent session
  def remember(user)
    user.remember
    cookies.permanent.signed[:user_id] = user.id
    cookies.permanent[:remember_token] = user.remember_token
  end

  # return the user corresponding to the remember token cookie.
  def current_user
    if (user_id = session[:user_id])
      @current_user ||= User.find_by(id: user_id)
    elsif (user_id = cookies.signed[:user_id])
      user = User.find_by(id: user_id)
      # if user && user.authenticated?(cookies[:remember_token])
      #   log_in user
      #   @current_user = user
      # end
      user&.authenticated?(cookies[:remember_token]) {
        log_in user
        @current_user = user
      }
    end
  end

  # return true if the user is logged-in
  def logged_in?
    !current_user.nil?
  end

  # forgets a persistent session
  def forget(user)
    user.forget
    cookies.delete(:user_id)
    cookies.delete(:remember_token)
  end

  # logout the current user
  def log_out
    forget(current_user)
    session.delete(:user_id)
    @current_user = nil
  end

  # redirect to stored location
  def redirect_back_to(default)
    redirect_to(session[:forwarding_url] || default)
    session.delete(:forwarding_url)
  end

  # store the url trying to access
  def store_location
    session[:forwarding_url] = request.original_url if request.get?
  end
end
