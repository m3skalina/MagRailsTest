# frozen_string_literal: true

class Movement < ApplicationRecord
  include Discard::Model
  belongs_to :user
  # belongs_to :products
  default_scope -> { order(created_at: :desc) }
  default_scope -> { kept }
  validates :product_id, presence: true
  validates :quantity, presence: true, numericality: { only_integer: true }
  validates :depot_id, presence: true
end
