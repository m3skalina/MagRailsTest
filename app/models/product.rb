# frozen_string_literal: true

class Product < ApplicationRecord
  include Discard::Model
  belongs_to :user
  has_many :movements, dependent: :destroy
  default_scope -> { order(product_code: :asc) }
  default_scope -> { kept }
  before_save { self.product_code = product_code.upcase }
  mount_uploader :picture, PictureUploader
  validates :user_id, presence: true
  validates :name, presence: true, length: { maximum: 100 }
  validates :price, presence: true, numericality: true
  validates :qtystock, presence: true, numericality: { only_integer: true }
  validates :product_code, presence: true, uniqueness: { case_sensitive: false }
  validate :validate_picture_size

  after_discard do
    movements.discard_all
  end

  after_undiscard do
    movements.undiscard_all
  end

  def getmovements
    Movement.where('products_id = ?', id)
  end

  def self.search(term)
    where('name ILIKE :term OR product_code ILIKE :term', term: "%#{term}%")
  end

  private

  # def picture_size
  #   if picture.size > 1.megabytes
  #     errors.add(:picture, "should be less than 1MB")
  #   end
  # end

  def validate_picture_size
    return if picture.size <= 1.megabytes

    errors.add(:picture, "should be less than 1MB")
  end
end
