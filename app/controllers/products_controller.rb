# frozen_string_literal: true

class ProductsController < ApplicationController
  before_action :logged_in_user, only: %i[index show new create edit update destroy]
  before_action :admin_user, only: :destroy

  def index
    respond_to do |format|
      format.html { @products = Product.paginate(page: params[:page], per_page: 2) }
      format.json { @products = Product.search(params[:term]) }
    end
  end

  def show
    @product = Product.find(params[:id])
    @movements = @product.movements
    # @movements = Movement.find_by(products_id: params[:id])
  end

  def new
    @product = Product.new
  end

  def create
    @product = current_user.products.build(product_params)
    if @product.save
      flash[:success] = 'Product saved'
      redirect_to products_url
    else
      render 'products/new'
    end
  end

  def edit
    @product = Product.find(params[:id])
  end

  def update
    @product = Product.find(params[:id])
    if @product.update_attributes(product_params)
      flash[:success] = "Product modified"
      redirect_to @product
    else
      render 'edit'
    end
  end

  def destroy
    Product.find(params[:id]).discard
    flash[:success] = "Product deleted"
    redirect_to products_url
  end

  private

  def product_params
    params.require(:product).permit(:name, :description, :price, :qtystock, :product_code, :picture)
  end

  def admin_user
    redirect_to(products_url) unless current_user.admin?
  end
end
