# frozen_string_literal: true

class MovementsController < ApplicationController
  before_action :logged_in_user, only: %i[new create]
  autocomplete :product, :name, full: true, limit: 5

  def new
    @movement = Movement.new
  end

  def create
    @movement = current_user.movements.build(movement_params)
    if @movement.save
      @new_qty_stock = Movement.where(product_id: @movement.product_id).sum(:quantity)
      @product = Product.find(@movement.product_id)
      @product&.update_attribute(:qtystock, @new_qty_stock)
      # if @product
      #   @product.update_attribute(:qtystock, @new_qty_stock)
      # end
      # @new_qty_stock = Movement.sum(:product_id, :conditions => {:product_id => [8]})
      flash[:success] = "Movement saved"
      redirect_to products_url
    else
      render 'movements/new'
    end
  end

  private

  def movement_params
    params.require(:movement).permit(:quantity, :product_id, :depot_id)
  end
end
