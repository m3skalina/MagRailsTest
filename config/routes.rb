# frozen_string_literal: true

Rails.application.routes.draw do
  root  'static_pages#home'
  get '/signup', to: 'users#new'
  get '/login', to: 'sessions#new'
  post  '/login', to: 'sessions#create'
  delete  '/logout', to:  'sessions#destroy'
  post  '/signup', to: 'users#create'
  # get   'static_pages/home'
  resources :users, only: [:new, :create, :show]
  resources :products, only: [:index, :show, :new, :create, :edit, :update, :destroy]
  resources :movements, only: [:new, :create, :destroy]
  resources :movements do
    get :autocomplete_product_name, :on => :collection
  end
end
